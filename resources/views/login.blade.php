<table>
    <form method="post" action="/">
        @csrf
        <tbody>
            <tr>
                <div class="form-group">
                    <td>
                        <label for="username">Nama</label>
                    </td>
                    <td>
                        <input type="text" class="form-control" id="uname" placeholder="masukkan username" name="username">
                    </td>
                </div>
            </tr>
            <tr>
                <div class="form-group">
                    <td>
                        <label for="password">Password</label>
                    </td>
                    <td>
                        <input type="password" class="form-control" id="pass" placeholder="masukkan password" name="password">
                    </td>
                </div>
            </tr>
            <td>
                <button class="submit" class="btn btn-primary">login</button>
            </td>
        </tbody>
    </form>
</table>
